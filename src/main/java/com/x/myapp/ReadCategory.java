package com.x.myapp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadCategory {
    public static void main(String[] args){
        if (args.length != 1) {
            System.out.println("Need one argument for input file name!");
            return;
        }
        String fileName = args[0];
        String line = null;
        List<Pair> list = new ArrayList<Pair>();
        int countPerson = 0;
        int countPlace = 0;
        int countAnimal = 0;
        int countComputer = 0;
        int countOther = 0;

        try {
            // FileReader reads text files in the default encoding
            FileReader fileReader = new FileReader(fileName);

            // Wrap FileReader in BufferedReader
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                String category = line.substring(0, line.indexOf(' '));
                String subCategory = line.substring(line.indexOf(' ') + 1);
                Pair pair = null;

                if (category.equals("PERSON")) {
                    pair = new Pair(category, subCategory);
                    if(!list.contains(pair)) {
                        list.add(pair);
                        countPerson++;
                    }
                }

                if (category.equals("PLACE")){
                    pair = new Pair(category, subCategory);
                    if(!list.contains(pair)) {
                        list.add(pair);
                        countPlace++;
                    }
                }

                if (category.equals("ANIMAL")){
                    pair = new Pair(category, subCategory);
                    if(!list.contains(pair)) {
                        list.add(pair);
                        countAnimal++;
                    }
                }

                if (category.equals("COMPUTER")){
                    pair = new Pair(category, subCategory);
                    if(!list.contains(pair)) {
                        list.add(pair);
                        countComputer++;
                    }
                }

                if (category.equals("OTHER")) {
                    pair = new Pair(category, subCategory);
                    if(!list.contains(pair)) {
                        list.add(pair);
                        countOther++;
                    }
                }

            }

            // Close files
            bufferedReader.close();

        } catch (FileNotFoundException ex){
            System.out.println("Unable to open file '" + fileName + "'");
        } catch (IOException ex){
            ex.printStackTrace();
        }

        System.out.println("CATEGORY     COUNT");
        System.out.printf("PERSON       %d\n", countPerson);
        System.out.printf("PLACE        %d\n", countPlace);
        System.out.printf("ANIMAL       %d\n", countAnimal);
        System.out.printf("COMPUTER     %d\n", countComputer);
        System.out.printf("OTHER        %d\n\n", countOther);

        for(Pair pair : list){
            System.out.println(pair.getCategory() + " " + pair.getSubCategory());
        }
    }
}
