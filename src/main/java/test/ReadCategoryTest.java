package test;

import com.x.myapp.ReadCategory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.junit.Assert.*;

public class ReadCategoryTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void testMain() throws Exception {
        String[] args = {"./src/main/resources/sample.txt"};
        ReadCategory.main(args);
        String expected = "CATEGORY     COUNT\n" +
                "PERSON       2\n" +
                "PLACE        2\n" +
                "ANIMAL       2\n" +
                "COMPUTER     1\n" +
                "OTHER        1\n" +
                "\n" +
                "PERSON Bob Jones\n" +
                "PLACE Washington\n" +
                "PERSON Mary\n" +
                "COMPUTER Mac\n" +
                "OTHER Tree\n" +
                "ANIMAL Dog\n" +
                "PLACE Texas\n" +
                "ANIMAL Cat\n";
        assertEquals(expected, systemOutRule.getLog());
    }
}