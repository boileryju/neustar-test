package com.x.myapp;

public class Pair {
    private String category;
    private String subCategory;

    public Pair(String category, String subCategory){
        this.category = category;
        this.subCategory = subCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair pair = (Pair) o;

        if (category != null ? !category.equals(pair.category) : pair.category != null) return false;
        return !(subCategory != null ? !subCategory.equals(pair.subCategory) : pair.subCategory != null);

    }

    @Override
    public int hashCode() {
        int result = category != null ? category.hashCode() : 0;
        result = 31 * result + (subCategory != null ? subCategory.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "category='" + category + '\'' +
                ", subCategory='" + subCategory + '\'' +
                '}';
    }
}
